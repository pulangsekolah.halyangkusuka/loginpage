package stockbit.test.page_object;

import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.By;
import stockbit.test.base.BasePageObject;

import java.util.Objects;

public class LoginPage extends BasePageObject {
    public void ClickLoginEntryPoint() {
        IsDisplayed("IMAGE_LOGIN");
        tap("BUTTON_WELCOME_LOGIN");
    }

    public void InputUsername(String username) {
        InputText("TEXT_BOX_USERNAME", username);
    }

    public void InputPassword(String password) {
        InputText("TEXT_BOX_PASSWORD", password);
    }

    public void ClickButtonLogin() {
        tap("BUTTON_LOGIN");
    }

    public void checkAlert(String alert) {
        isTextSame("TEXT_ALERT", alert);
    }

    public void headerDisplayed() {
        IsDisplayed("HEADER_AFTER_LOGIN");
    }

}
