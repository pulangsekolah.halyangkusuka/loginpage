package stockbit.test.base;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stockbit.test.android_driver.AndroidDriverInstance;
import stockbit.test.utils.Constants;
import stockbit.test.utils.Utils;

import java.util.Objects;

public class BasePageObject {
    Dotenv env = Dotenv.load();

    public AndroidDriver getDriver() {
        return AndroidDriverInstance.androidDriver;
    }

    public AndroidElement waitStrategy(ExpectedCondition<WebElement> conditions) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Constants.TIMEOUT);
        return (AndroidElement) wait.until(conditions);
    }

    public AndroidElement waitUntilClickable(By element) {
        return waitStrategy(ExpectedConditions.elementToBeClickable(element));
    }

    public AndroidElement waitUntilVisible(By element) {
        return waitStrategy(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);

        if (elementValue == null) {
            throw new NoSuchElementException("Element not found!" + elementLocator);
        } else {
            String[] locator = elementValue.split("_"); // [type, element]
            String locatorType = locator[0]; //type
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1); //element

            switch (locatorType) {
                case "id":
                    if (Objects.equals(env.get("ENV"), "prod")) {
                        return By.id("com.stockbit.android:id/" + locatorValue);
                    } else {
                        return By.id("com.stockbitdev.android:id/" + locatorValue);
                    }
                case "xpath":
                    return By.xpath(locatorValue);
                case "accessibilityId":
                    return MobileBy.AccessibilityId(elementLocator);
                default:
                    throw new IllegalStateException("Unexpected value: " + locatorType);
            }
        }
    }

    public void InputText(String locator, String text) {
        waitUntilVisible(element(locator)).sendKeys(text);
    }

    public void tap(String locator) {
        waitUntilClickable(element(locator)).click();
    }

    public void IsDisplayed(String locator) {
        By element = element(locator);
        try {
            waitUntilVisible(element);
        } catch (TimeoutException error) {
            Utils.printError(String.format("Element [%s] not found!", element));
        }
    }

    public void isTextSame(String locator, String expectedText) {
        By element = element(locator);
        String actualText = waitUntilVisible(element).getText();
        if (!actualText.equals(expectedText)) {
            Utils.printError(String.format("Expected string [%s] is not match with actual string [%s]", expectedText, actualText));
        }
    }

}
