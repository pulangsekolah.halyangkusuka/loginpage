Feature: Login feature

  Scenario: Login with invalid password
    Given User click entry point login
    When User input username "username"
    And User input password "password99988"
    And User click login button
    Then User see alert "Username atau password salah. Mohon coba lagi." in login page

  Scenario: Login with invalid username
    Given User click entry point login
    When User input username "username99988"
    And User input password "password"
    And User click login button
    Then User see alert "Username atau password salah. Mohon coba lagi." in login page

  Scenario: Login with blank username and password
    Given User click entry point login
    When User input username ""
    And User input password ""
    And User click login button
    Then User see alert "Mohon masukkan username/email dan password kamu." in login page

  Scenario: Successfully Login
    Given User click entry point login
    When User input username "lordlord"
    And User input password "stockbit"
    And User click login button
    Then Header will be appear