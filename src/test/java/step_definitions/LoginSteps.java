package step_definitions;

import io.cucumber.java8.En;
import stockbit.test.page_object.LoginPage;

public class LoginSteps implements En {
    LoginPage loginPage = new LoginPage();

    public LoginSteps() {
        Given("^User click entry point login$", () ->
                loginPage.ClickLoginEntryPoint());

        And("User input username {string}", (String username) ->
                loginPage.InputUsername(username));

        When("^User input password \"([^\"]*)\"$", (String password) ->
                loginPage.InputPassword(password));

        And("^User click login button$", () ->
                loginPage.ClickButtonLogin());

        Then("^User see alert \"([^\"]*)\" in login page$", (String alert) -> {
            loginPage.checkAlert(alert);
        });

        Then("^Header will be appear$", () -> loginPage.headerDisplayed());

    }
}
