List dependencies dan fungsinya:
1. io.appium:java-client:7.5.1 = digunakan untuk test automation pada aplikasi mobile menggunakan Appium.
2. io.cucumber:cucumber-java8:7.12.0 = untuk mengintegrasikan framework pengujian BDD Cucumber dengan Java 8.
3. io.cucumber:cucumber-junit:7.12.0 = untuk menjalankan skenario pengujian Cucumber menggunakan JUnit.
4. org.junit.jupiter:junit-jupiter:5.8.2 = untuk mengintegrasikan framework pengujian JUnit Jupiter.
5. junit:junit:4.13.2 = untuk mengintegrasikan framework pengujian JUnit versi 4.
6. org.junit.jupiter:junit-jupiter-api:5.8.2 = untuk mengembangkan dan mengeksekusi pengujian menggunakan JUnit Jupiter.
7. io.cucumber:cucumber-junit-platform-engine:7.12.0 = untuk menjalankan skenario pengujian Cucumber di atas platform JUnit 5.
8. io.github.cdimascio:java-dotenv:5.2.2 = untuk membaca variabel lingkungan (environment variables) dari file .env dalam proyek Java.

File JDK(1.8_211) :

- Mac: https://drive.google.com/file/d/1u_UCGydGm54UP1WWTkKhJfoJwXBm_hMe/view

- Windows: https://drive.google.com/file/d/1UivUV31ILl6gnf3fU-HrXL2aOIR8lGh3/view

Cara setup JDK  :

- Windows : https://www.youtube.com/watch?v=GcG2nzYn1PA&t=59s&ab_channel=RomaRomanik

Setup Intellij :

- https://www.jetbrains.com/idea/download/#section=windows

Cara setup Appium :

- Mac https://www.youtube.com/watch?v=7APcLr-cBM8&list=PLhW3qG5bs-L8npSSZD6aWdYFQ96OEduhk&index=5&ab_channel=AutomationStepbyStep

- Windows https://www.youtube.com/watch?v=x-hBpgM5je8&list=PLhW3qG5bs-L8npSSZD6aWdYFQ96OEduhk&index=3&ab_channel=AutomationStepbyStep

Cara setup Android Emulator :

- Mac https://www.youtube.com/watch?v=hTk2DojxcEM

- Windows https://www.youtube.com/watch?v=fgvoQWIiTyo